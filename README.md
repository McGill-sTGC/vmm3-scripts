# VMM3 Scripts

The scripts here are based on Liang Guan's: https://gitlab.cern.ch/lguan/ATLAS_NSW_sTGC_miniDAQ_Firmware/tree/master/scripts.

Note that these scripts have been tuned to communicate with a Tektronix (DPO 3034 Digital Phosphor) oscilloscope. If you have a LeCroy scope, let me know and we can share code written by a colleague with you.

The purpose of these scripts is to perform noise (RMS of the analog baseline) measurements of sTGC modules. The first step is to edit vmm.xml; it is important that all VMM channels in this file are set to be masked, i.e. "\<sm\>1\</sm\>".

We edited vmm_measlib.py, in particular the function noise_level_scan(), lines 208-230, such that only a single VMM channel is unmasked at a time.

To run, turn on the mini-DAQ system and power a single front-end board (FEB) loaded with VMM3s and attached to a module. Connect a LEMO cable from the desired MMCX connector to a channel on your oscilloscope. Next, perform the following steps:

1. Using sTGC_readout_sw, fully configure the board and VMMs.
2. Mask every channel on every VMM making sure to reconfigure the VMM.
3. Close sTGC_readout_sw.
4. In vmm3-scripts/, open main.py and edit
    - scope_chan to be the channel on the scope that the MMCX is connected to;
    - meas_id to something like "noise_QS3P6_pFEB11_Feb_19_2019";
    - vmmid to be the VMM ID, e.g. "a", "b", or "c";
    - ensure that vmm_pt=50 and vmm_gain=1.0 in noise_level_scan().
5. Open target_info.py and set board_id to be the e-link that the FEB is connected to.
6. In vmm.xml, note that sp=0 (negative polarity) should be used for wires and sp=1 (positive polarity) for pads (and strips) and set accordingly.
7. Run: "./main.py"

After completion, a file is saved containing the noise measurements for each VMM channel in addition to being outputted onto the screen.

Next, move onto the next VMM on the FEB and repeat steps 1-7.