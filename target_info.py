
## Place holder to store asic configuration parameters

from xmlparser import XmlParser
xp = XmlParser()


asic_type_dict={'VMM':0,'TDS':1,'ROC':2,'NONE':3}

class target(object):
	def __init__(self,chip):
		self.guest_ip = "192.168.0.1"
		self.guest_port = 6008
		self.board_type = 1 #0:pFEBs or 1:sFEBs
		self.board_id = 5 #e-link number
		if (chip=="vmm"):
			self.asic_type = asic_type_dict['VMM']
                        #For pFEBs, uncomment line 20 and comment 21
                        #For sFEBs, uncomment line 21 and comment 20
			#self.asic_id = 4 #1:VMMa; 2:VMMb; 4:VMMc
                        self.asic_id = 255 #255:Configures all 8 VMMs
			self.cmd_content=xp.vmm_hexgen("vmm.xml")
		elif (chip=="tds"):
			self.asic_type = asic_type_dict['TDS']
			self.asic_id = 0
			self.cmd_content=xp.tds_hexgen("tds.xml")
		else: 
			self.asic_type = asic_type_dict['NONE']
			self.asic_id = 0
			self.cmd_content=''
			
